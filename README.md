"ASSEMBLER for NOOBS"

# What is Handy?
	-Handy is an interpreted, high-level, experimental programming language for beginners that allows you to create simple console apps.
	Just create file "example.hn" and type (while in source folder): 
		"./handy example.hn" - on Linux,
	 	"handy example.hn" - on Windows.

# How does it works?
	-Handy provides its fast and safe API to C programming language. All commands divided into lines (like Assembly language) that makes code more readable and compact.
	Executable file is just an interpeter server. You can learn more about Handy's syntax at <link>.

# Why I should use Handy?
	-The project's main purpose - create a simple language for learning programming, in elementary shool, for example.
	Secondary purpose - just for fun, as Python was once created.

# Might Handy be better than other languages?
	-Partially, yeah. You can read about its advantages at <link>.